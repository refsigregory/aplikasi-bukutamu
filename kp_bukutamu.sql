-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 23, 2019 at 06:53 PM
-- Server version: 10.1.34-MariaDB-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kp_bukutamu`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_appointment`
--

CREATE TABLE `tb_appointment` (
  `id_appointment` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_user_pimpinan` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `jam` time NOT NULL,
  `keterangan` text NOT NULL,
  `foto_identitas` varchar(100) NOT NULL,
  `status_permintaan` set('pending','accepted','denied') NOT NULL,
  `id_typeofneeds` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_job`
--

CREATE TABLE `tb_job` (
  `id_job` int(11) NOT NULL,
  `jabatan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_job`
--

INSERT INTO `tb_job` (`id_job`, `jabatan`) VALUES
(1, 'Kepala Dinas'),
(2, 'Kepala Bagian'),
(3, 'Sekretaris');

-- --------------------------------------------------------

--
-- Table structure for table `tb_notification`
--

CREATE TABLE `tb_notification` (
  `id_notification` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `pesan` text NOT NULL,
  `status_dibaca` tinyint(1) NOT NULL DEFAULT '0',
  `waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tb_typeofneeds`
--

CREATE TABLE `tb_typeofneeds` (
  `id_typeofneeds` int(11) NOT NULL,
  `jenis_keperluan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_typeofneeds`
--

INSERT INTO `tb_typeofneeds` (`id_typeofneeds`, `jenis_keperluan`) VALUES
(1, 'Testing');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `id_usertype` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jenis_kelamin` set('Laki-laki','Perempuan') NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telepon` varchar(15) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `asal_instansi` varchar(50) DEFAULT NULL,
  `id_job` int(11) DEFAULT NULL,
  `status_keberadaan` set('ada','keluar','sibuk') NOT NULL DEFAULT 'ada',
  `kode` varchar(100) NOT NULL,
  `status_pendaftaran` set('terverifikasi','menunggu') NOT NULL DEFAULT 'terverifikasi'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `id_usertype`, `username`, `password`, `nama`, `jenis_kelamin`, `email`, `telepon`, `alamat`, `asal_instansi`, `id_job`, `status_keberadaan`, `kode`, `status_pendaftaran`) VALUES
(1, 1, 'admin', 'admin', 'Administrator', '', NULL, NULL, NULL, NULL, NULL, 'ada', '', 'terverifikasi');

-- --------------------------------------------------------

--
-- Table structure for table `tb_usertype`
--

CREATE TABLE `tb_usertype` (
  `id_usertype` int(11) NOT NULL,
  `tipe_user` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_usertype`
--

INSERT INTO `tb_usertype` (`id_usertype`, `tipe_user`) VALUES
(1, 'Admin'),
(2, 'Tamu'),
(3, 'Tamu Terdaftar'),
(4, 'Pimpinan');

-- --------------------------------------------------------

--
-- Table structure for table `tb_visitation`
--

CREATE TABLE `tb_visitation` (
  `id_visitation` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `asal_instansi` varchar(50) NOT NULL,
  `jenis_kelamin` varchar(50) NOT NULL,
  `telepon` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `jam_masuk` time NOT NULL,
  `jam_keluar` time NOT NULL,
  `foto_identitas` varchar(50) NOT NULL,
  `foto_berkas` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  `status_respon` set('pending','finished','canceled') NOT NULL DEFAULT 'pending',
  `id_user_pimpinan` int(11) NOT NULL,
  `id_appointment` int(11) DEFAULT NULL,
  `id_typeofneeds` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_appointment`
--
ALTER TABLE `tb_appointment`
  ADD PRIMARY KEY (`id_appointment`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_typeofneeds` (`id_typeofneeds`),
  ADD KEY `id_pimpinan` (`id_user_pimpinan`);

--
-- Indexes for table `tb_job`
--
ALTER TABLE `tb_job`
  ADD PRIMARY KEY (`id_job`);

--
-- Indexes for table `tb_notification`
--
ALTER TABLE `tb_notification`
  ADD PRIMARY KEY (`id_notification`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tb_typeofneeds`
--
ALTER TABLE `tb_typeofneeds`
  ADD PRIMARY KEY (`id_typeofneeds`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_usertype` (`id_usertype`),
  ADD KEY `id_jabatan` (`id_job`);

--
-- Indexes for table `tb_usertype`
--
ALTER TABLE `tb_usertype`
  ADD PRIMARY KEY (`id_usertype`);

--
-- Indexes for table `tb_visitation`
--
ALTER TABLE `tb_visitation`
  ADD PRIMARY KEY (`id_visitation`),
  ADD KEY `id_appointment` (`id_appointment`),
  ADD KEY `id_typeofneeds` (`id_typeofneeds`),
  ADD KEY `id_pimpinan` (`id_user_pimpinan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_appointment`
--
ALTER TABLE `tb_appointment`
  MODIFY `id_appointment` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_job`
--
ALTER TABLE `tb_job`
  MODIFY `id_job` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_notification`
--
ALTER TABLE `tb_notification`
  MODIFY `id_notification` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_typeofneeds`
--
ALTER TABLE `tb_typeofneeds`
  MODIFY `id_typeofneeds` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_usertype`
--
ALTER TABLE `tb_usertype`
  MODIFY `id_usertype` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_visitation`
--
ALTER TABLE `tb_visitation`
  MODIFY `id_visitation` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_appointment`
--
ALTER TABLE `tb_appointment`
  ADD CONSTRAINT `tb_appointment_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`),
  ADD CONSTRAINT `tb_appointment_ibfk_2` FOREIGN KEY (`id_typeofneeds`) REFERENCES `tb_typeofneeds` (`id_typeofneeds`),
  ADD CONSTRAINT `tb_appointment_ibfk_3` FOREIGN KEY (`id_user_pimpinan`) REFERENCES `tb_user` (`id_user`);

--
-- Constraints for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD CONSTRAINT `tb_user_ibfk_1` FOREIGN KEY (`id_usertype`) REFERENCES `tb_usertype` (`id_usertype`),
  ADD CONSTRAINT `tb_user_ibfk_2` FOREIGN KEY (`id_job`) REFERENCES `tb_job` (`id_job`);

--
-- Constraints for table `tb_visitation`
--
ALTER TABLE `tb_visitation`
  ADD CONSTRAINT `tb_visitation_ibfk_2` FOREIGN KEY (`id_typeofneeds`) REFERENCES `tb_usertype` (`id_usertype`),
  ADD CONSTRAINT `tb_visitation_ibfk_3` FOREIGN KEY (`id_user_pimpinan`) REFERENCES `tb_user` (`id_user`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
