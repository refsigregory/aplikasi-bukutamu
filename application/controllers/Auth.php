<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller 
{

    public function __construct()
    {
        parent::__construct();

        // auto load model
        $this->load->model('M_auth');
        $this->load->model('M_pengguna');
    }

    public function index()
    {
        // memanggil fungsi cek  login
        $this->M_auth->check();
    }

    public function login()
    {
        $this->M_auth->logged_in();
        $data['judul']='Halaman Login';
        $this->load->view('templates/header',$data);
        $this->load->view('auth/v_login');
        $this->load->view('templates/footer');
    }

    public function register()
    {
        $this->M_auth->logged_in();
        $data['judul']='Halaman Register';
        $this->load->view('templates/header',$data);
        $this->load->view('auth/v_register');
        $this->load->view('templates/footer');
    }

    public function tamu()
    {
        $this->M_auth->check();
        $this->M_auth->blockRole("admin");
        $this->M_auth->blockRole("pengguna");
        $this->M_auth->blockRole("pimpinan");
        $data['judul']='Halaman Tamu';
        $this->load->view('templates/header',$data);
        
        $data['pimpinan'] = $this->M_tamu->getPimpinan();
        $data['keperluan'] = $this->M_tamu->getKeperluan();
        $data['daftar_tamu'] = $this->M_tamu->getListTodayAll();
        $this->load->view('auth/v_tamu',$data);

        if(isset($_GET['show_list']))
        {
            $data['daftar_tamu'] = $this->M_tamu->getListByKepalaToday($_GET['show_list']);
            $this->load->view('admin/modal_list_tamu.php',$data);
        } else {
            
        }

        $this->load->view('templates/footer');
    }

    public function newTamu()
    {
        $this->M_auth->check();
        $data = $this->input->post();
        //print_r($data);

        if($this->input->post('nama') != '') {
            $id = $this->input->get('id');
            $image = $data['image'];

            unset($data['image']);

            $folderPath = "uploads/";

  

            $image_parts = explode(";base64,", $image);
        
            $image_type_aux = explode("image/", $image_parts[0]);
        
            $image_type = $image_type_aux[1];
        
          
        
            $image_base64 = base64_decode($image_parts[1]);
        
            $fileName = uniqid() . '.png';
        
          
        
            $file = $folderPath . $fileName;
        
            file_put_contents($file, $image_base64);
        

			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
            }
            
            if($this->m_pengguna->getKepalaByIDUser($data['id_user_pimpinan'])[0]->status_keberadaan != 'ada') {
                $err[] = "Pimpinan sedang " . $this->m_pengguna->getKepalaByIDUser($data['id_user_pimpinan'])[0]->status_keberadaan;
            }

			if(!isset($err))
			{
                $data = array_merge($data, ["foto_identitas"=>$fileName, "jam_masuk" => date("H:i:s"), "status_respon" => 'pending']);
                $query = $this->db->insert("tb_visitation", $data);
                redirect('auth/tamu?show_list='.$data['id_user_pimpinan']);
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('auth/tamu?error='.$msg);
			}
		}else {
			$msg = "Informasi tidak lengkap";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('auth/tamu?error='.$msg);
		}
    }


    public function process()
    {
        // cek login
        $this->M_auth->logged_in();
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        // memanggil fungsi cek login
        $this->M_auth->login($username, $password);
    }

    public function process_register()
    {
        $data = $this->input->post();
		if($this->input->post('username') != '') {
            $id = $this->input->get('id');
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
                $data = array_merge($data, ["id_usertype"=>3, 'status_pendaftaran' => 'menunggu']);
                $query = $this->db->insert("tb_user", $data);
                $this->sendMail($data['email'], $this->db->insert_id());
                $msg = "Pendaftaran berhasil! Silahkan link konfirmasi di email anda";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('auth/login', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('auth/login?error='.$msg, 'refresh');
			}
		}else {
			$msg = "Data Pengguna tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('auth/register?error='.$msg, 'refresh');
		}
    }


    public function logout()
    {
        // Funsi Logout
        $this->session->sess_destroy();
        redirect('auth/login');
    }

    public function checkDataTamu()
    {
        if(isset($_GET['id']))
      {
        $id = $this->input->get('id');
      } else { $id = 0; }

        switch($this->input->get('role')):
            case 'admin':
                echo $this->m_tamu->getCountListTodayAll();
            break;

            case 'pimpinan':
                echo $this->m_tamu->getCountListTodayByKepala($id);
            break;

            default:
                echo 0;
            break;
        endswitch;
    }

    public function checkNotif()
    {
        $id = $this->input->get('id');
        echo $this->m_pengguna->getCountNotifikasiByIDUser($id);
    }

    public function testEmail()
    {
        $this->sendMail('refsisangkay@gmail.com', '4');
    }
    
    public function sendMail($email, $id) {
        $this->load->library('email');
        
        $kode = base64_encode(time());
        $subject = 'Aktivasi Akun';
        $message = '<p>Aktivasi akun anda dengan mengunjungi link berikut <a href="'.base_url('auth/activate/?id='.$id.'&kode='.$kode).'">Aktifkan Akun</a></p>';
        
        // Get full html:
        $body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
            <title>' . html_escape($subject) . '</title>
            <style type="text/css">
                body {
                    font-family: Arial, Verdana, Helvetica, sans-serif;
                    font-size: 16px;
                }
            </style>
        </head>
        <body>
        ' . $message . '
        </body>
        </html>';
        // Also, for getting full html you may use the following internal method:
        //$body = $this->email->full_html($subject, $message);
        
        if(isset($email)) {
            $this->db->where("id_user", $id);
            $query = $this->db->update("tb_user", ["kode"=>$kode]);
            $result = $this->email
                    ->from('refsisangkay@gmail.com')
                    //->reply_to('yoursecondemail@somedomain.com')    // Optional, an account where a human being reads.
                    ->to($email)
                    ->subject($subject)
                    ->message($body)
                    ->send();
            return $result;
        }
        //var_dump($result);
        //echo '<br />';
        echo $this->email->print_debugger();
        
        //exit;
            }
        
        public function activate()
        {
            $id = $this->input->get('id');
            $kode = $this->input->get('kode');

            $user = $this->m_pengguna->getByID($id)[0];
            if($user->kode == $kode):
                $this->db->where("id_user", $id);
                $query = $this->db->update("tb_user", ["status_pendaftaran"=>'terverifikasi']);
                if($query)
                {
                    $msg = "Akun anda berhasil diverifikasi. Masuk ke akun ke akun anda sekarang.";
                    $this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
                    redirect('auth/login?success='.$msg, 'refresh');
                }else {
                    $msg = "Terjadi kesalahan";
                    $this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
                    redirect('auth/login?error='.$msg, 'refresh');
                }
            else:
                $msg = "Kode Aktivasi Salah";
			    $this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			    redirect('auth/login?error='.$msg, 'refresh');
            endif;
        }

}
