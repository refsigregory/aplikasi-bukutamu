<?php

require './vendor/autoload.php';
class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('M_auth');
        $this->load->model('M_tamu');
        $this->load->model('M_pengguna');

        $this->M_auth->check();
        
        $this->M_auth->blockRole("tamu");
    }

    public function index()
    {
        $head['judul']='Dashboard';
        $data['notif'] = $this->m_pengguna->getNotifikasiByIDUser($this->session->userdata('id'));

        $this->load->view('templates/header',$head);
        $this->load->view('templates/sidebar',$head);
        $this->load->view('admin/dashboard',$data);
        $this->load->view('templates/footer');
    }

    public function pengguna()
    {
        $head['judul']='Pengguna';
        $data['pengguna'] = $this->M_pengguna->getAll();

        $this->load->view('templates/header',$head);
        $this->load->view('templates/sidebar',$head);
        $this->load->view('admin/pengguna', $data);
        if(isset($_GET['detail']))
        {
            $id = $this->input->get('detail');
            $pengguna =  $this->M_pengguna->getByID($id)[0];
            
            if($pengguna->id_usertype == 1)
            {
                $admin = $this->M_pengguna->getAdminByIDUser($id);
                if($admin != ""):
                    $data['detail'] = $admin[0];
                    $this->load->view('admin/modal_pengguna_admin', $data);
                endif;
            }else if($pengguna->id_usertype == 4)
            {
                $kepala = $this->M_pengguna->getKepalaByIDUser($id);
                if($kepala != ""):
                    $data['detail'] = $kepala[0];
                    $this->load->view('admin/modal_pengguna_kepala', $data);
                endif;
            }else if($pengguna->id_usertype == 3)
            {
                $pengguna = $this->M_pengguna->getPenggunaByIDUser($id);
                if($pengguna != ""):
                    $data['detail'] = $pengguna[0];
                    $this->load->view('admin/modal_pengguna_pengguna', $data);
                endif;
            } else if($pengguna->id_usertype == 2)
            {
                $tamu = $this->M_pengguna->getByID($id);
                if($tamu != ""):
                    $data['detail'] = $tamu[0];
                    $this->load->view('admin/modal_pengguna_tamu', $data);
                endif;
            }
        }
        

        $this->load->view('templates/footer');
    }

    public function tambah_pengguna()
    {
        $head['judul']='Tambah Pengguna';
        
        $this->load->view('templates/header',$head);
        $this->load->view('templates/sidebar',$head);
        $this->load->view('admin/tambah_pengguna');
        $this->load->view('templates/footer');
    }

    public function tambahPengguna()
    {
        $data = $this->input->post();
		if($this->input->post('id_usertype') != '') {
            $id = $this->input->get('id');
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				$this->M_pengguna->tambah($data);
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('admin/tambah_pengguna?error='.$msg, 'refresh');
			}
		}else {
			$msg = "Data Pengguna tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('admin/tambah_pengguna?error='.$msg, 'refresh');
		}
    }

    public function edit_pengguna()
    {
        $head['judul']='Ubah Pengguna';
        
        $this->load->view('templates/header',$head);
        $this->load->view('templates/sidebar',$head);

        $data['data'] = $this->M_pengguna->getByID($this->input->get('id'))[0];
        $this->load->view('admin/edit_pengguna', $data);
        $this->load->view('templates/footer');
    }

    public function editPengguna()
    {
        $data = $this->input->post();
		if($this->input->post('id_user') != '') {
            $id = $this->input->post('id_user');
            unset($data['id_user']);
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				$this->M_pengguna->edit($id,$data);
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('admin/edit_pengguna?id='.$id.'&error='.$msg, 'refresh');
			}
		}else {
			$msg = "Data Pengguna tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('admin/edit_pengguna?id='.$this->input->post('id').'&error='.$msg, 'refresh');
		}
    }

    public function hapus_pengguna()
    {
        if(!empty($this->uri->segment(3))) {
			$id = $this->uri->segment(3);
			$this->M_pengguna->delete($id);
			$msg = "Data Pengguna berhasil hapus";
			$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
			redirect('admin/pengguna', 'refresh');
		}else {
			$msg = "Data Pengguna tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('admin/pengguna?error=ID tidak ditemukan', 'refresh');
		}
    }

    public function appointment()
    {
        $head['judul']='Appointment';
        
        switch($this->session->userdata('role')):
            case 'admin':
                $data['daftar_janji'] = $this->M_tamu->getListAppointment();
            break;
            case 'pimpinan':
                $data['daftar_janji'] = $this->M_tamu->getListAppointmentByKepala($this->session->userdata('id_user'));
            break;
            case 'pengguna':
                $data['daftar_janji'] = $this->M_tamu->getListAppointmentByPengguna($this->session->userdata('id_user'));
            break;
            default:
                $data['daftar_janji'] = "";
            break;
        endswitch;
        
        $this->load->view('templates/header',$head);
        $this->load->view('templates/sidebar',$head);
        $this->load->view('admin/appointment', $data);
        $this->load->view('templates/footer');
    }

    public function new_appointment()
    {
        $head['judul']='Buat Appointment';
        $data['pimpinan'] = $this->M_tamu->getPimpinan();
        $data['pengguna'] = $this->M_pengguna->getPengguna();
        $data['keperluan'] = $this->M_tamu->getKeperluan();
        
        $this->load->view('templates/header',$head);
        $this->load->view('templates/sidebar',$head);
        $this->load->view('admin/buat_appointment', $data);
        $this->load->view('templates/footer');
    }

    public function tambahAppointment()
    {
        $data = $this->input->post();
        //print_r($data);

        if($this->input->post('id_user') != '') {
            $id = $this->input->get('id');
            $image = $data['image'];

            unset($data['image']);

            $folderPath = "uploads/";

  

            $image_parts = explode(";base64,", $image);
        
            $image_type_aux = explode("image/", $image_parts[0]);
        
            $image_type = $image_type_aux[1];
        
          
        
            $image_base64 = base64_decode($image_parts[1]);
        
            $fileName = uniqid() . '.png';
        
          
        
            $file = $folderPath . $fileName;
        
            file_put_contents($file, $image_base64);
        

			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{

                $data = array_merge($data, ["foto_identitas"=>$fileName, 'status_permintaan'=> 'pending']);
                $query = $this->db->insert("tb_appointment", $data);
                redirect('admin/appointment');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('admin/new_appointment?error='.$msg, 'refresh');
			}
		}else {
			$msg = "Informasi tidak lengkap tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('admin/new_appointment?error='.$msg, 'refresh');
		}
    }

    public function terimaAppointment()
    {
        $id = $this->input->get('id');
        $status = "accepted";
        $q=$this->M_tamu->setAppointment($status, $id);
        $data = $this->M_tamu->getAppointmentByID($id);
        if($q)
        {   
            if($data != ""):
                $pengguna = $this->M_pengguna->getPenggunaByID($data[0]->id_user);
                $tamu = [
                    "id_user_pimpinan" => $data[0]->id_user_pimpinan,
                    "nama" => $pengguna[0]->nama,
                    "telepon" => $pengguna[0]->telepon,
                    "jenis_kelamin" => $pengguna[0]->jenis_kelamin,
                    "tanggal" =>  $data[0]->tanggal,
                    "jam_masuk" => $data[0]->jam,
                    "asal_instansi" => $pengguna[0]->asal_instansi,
                    "id_typeofneeds" => $data[0]->id_typeofneeds,
                    "keterangan" => $data[0]->keterangan,
                    "foto_identitas" => $data[0]->foto_identitas,
                    "id_appointment" => $data[0]->id_appointment
                ];
                $query = $this->db->insert("tb_visitation", $tamu);
                $this->m_pengguna->insertNotification($data[0]->id_user, "Appointment anda untuk ".$data[0]->keterangan." telah diterima");
                redirect('admin/appointment');
            else:
                redirect('admin/appointment?error=tamu gagagl ditambah');
            endif;
        }else {
            $msg = "Terjadi Kesalahan";
            $this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
            redirect('admin/appointment?error='.$msg, 'refresh');
        }
    }

    public function tolakAppointment()
    {
        $id = $this->input->get('id');
        $status = "denied";
        $q=$this->M_tamu->setAppointment($status, $id);
        $data = $this->M_tamu->getAppointmentByID($id);
        if($q)
        {
            $this->m_pengguna->insertNotification($data[0]->id_user, "Appointment anda ditolak");
            redirect('admin/appointment');
        }else {
            $msg = "Terjadi Kesalahan";
            $this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
            redirect('admin/appointment?error='.$msg, 'refresh');
        }
    }

    public function tamu()
    {
        $head['judul']='Daftar Tamu';
        
        $this->load->view('templates/header',$head);
        $this->load->view('templates/sidebar',$head);

        $data['pimpinan'] = $this->M_tamu->getPimpinan();
        switch($this->session->userdata('role')):
            case 'admin':
                $data['daftar_tamu'] = $this->M_tamu->getListAll();
                $data['tamu'] = $this->M_tamu->getLastToday();
            break;
            case 'pimpinan':
                $data['daftar_tamu'] = $this->M_tamu->getListByKepala($this->session->userdata('id_user'));
                $data['tamu'] = $this->M_tamu->getLastTodayByKepala($this->session->userdata('id_user'));
            break;
            default:
                $data['daftar_tamu'] = "";
                $data['tamu'] = "";
            break;
        endswitch;

        $this->load->view('admin/tamu', $data);

        if(isset($_GET['show']))
        {
            $data['detail'] = $this->m_tamu->getByID($this->input->get('show'))[0];
            $this->load->view('admin/modal_detail_pengunjung.php',$data);
        } else {

        }

        if(isset($_GET['berkas']))
        {
            $data['id'] = $this->input->get('berkas');
            $this->load->view('admin/modal_upload_berkas.php',$data);
        }

        $this->load->view('templates/footer');
    }

    public function tambah_tamu()
    {
        $head['judul']='Tambah Tamu';
        $data['pimpinan'] = $this->M_tamu->getPimpinan();
        $data['keperluan'] = $this->M_tamu->getKeperluan();
        
        $this->load->view('templates/header',$head);
        $this->load->view('templates/sidebar',$head);
        $this->load->view('admin/tambah_tamu', $data);
        $this->load->view('templates/footer');
    }

    public function tambahTamu()
    {
        $data = $this->input->post();
        //print_r($data);

        if($this->input->post('nama') != '') {
            $id = $this->input->get('id');
            $image = $data['image'];

            unset($data['image']);

            $folderPath = "uploads/";

  

            $image_parts = explode(";base64,", $image);
        
            $image_type_aux = explode("image/", $image_parts[0]);
        
            $image_type = $image_type_aux[1];
        
          
        
            $image_base64 = base64_decode($image_parts[1]);
        
            $fileName = uniqid() . '.png';
        
          
        
            $file = $folderPath . $fileName;
        
            file_put_contents($file, $image_base64);
        

			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
                $data = array_merge($data, ["foto_identitas"=>$fileName, "status_respon" => 'pending']);
                $query = $this->db->insert("tb_visitation", $data);
                redirect('admin/tamu?show_list&id='.$data['id_user_pimpinan']);
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('admin/tamu?error='.$msg, 'refresh');
			}
		}else {
			$msg = "Informasi tidak lengkap";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('admin/tamu?error='.$msg, 'refresh');
		}
    }

    public function setKeberadaan()
    {
        $id = $this->input->post('id_user_pimpinan');
        $status = $this->input->post('status_keberadaan');
        $q=$this->M_tamu->setStatusKeberadaan($status, $id);
        if($q)
        {   
            if(true):
                redirect('admin/tamu');
            else:
                redirect('admin/tamu?error=gaga mengatur status');
            endif;
        }else {
            $msg = "Terjadi Kesalahan";
            $this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
            redirect('admin/tamu?error='.$msg, 'refresh');
        }
    }

    public function selesaiTamu()
    {
        $id = $this->input->get('id');
        $status = "finished";
        $q=$this->M_tamu->setStatus($status, $id);
        $data = $this->M_tamu->getByID($id);
        if($q)
        {   
            if($data != ""):
                //$pengguna = $this->M_pengguna->getPenggunaByID($data[0]->id_pengguna);
                /*$tamu = [
                    "id_user_pimpinan" => $data[0]->id_user_pimpinan,
                    "nama" => $data[0]->nama,
                    "telepon" => $data[0]->telepon,
                    "jenis_kelamin" => $data[0]->jenis_kelamin,
                    "tanggal" =>  $data[0]->tanggal,
                    "jam_masuk" => $data[0]->jam_masuk,
                    "jam_keluar" => $data[0]->jam_keluar,
                    "asal_instansi" => $data[0]->asal_instansi,
                    "id_typeofneeds" => $data[0]->id_typeofneeds,
                    "keterangan" => $data[0]->keterangan,
                    "foto_berkas" => $data[0]->foto_berkas,
                    "foto_identitas" => $data[0]->foto_identitas,
                    "status_respon" => $data[0]->status_respon
                ];*/
                $query = $this->db->update($id, ["jam_keluar" => date("H:i")]);
                redirect('admin/tamu');
            else:
                redirect('admin/tamu?error=riwayat gagal ditambah');
            endif;
        }else {
            $msg = "Terjadi Kesalahan";
            $this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
            redirect('admin/tamu?error='.$msg, 'refresh');
        }
    }

    public function cancelTamu()
    {
        $id = $this->input->get('id');
        $status = "canceled";
        $q=$this->M_tamu->setStatus($status, $id);
        $data = $this->M_tamu->getByID($id);
        if($q)
        {   
            if($data != ""):
                //$pengguna = $this->M_pengguna->getPenggunaByID($data[0]->id_pengguna);
                /*$tamu = [
                    "id_pimpinan" => $data[0]->id_pimpinan,
                    "nama_tamu" => $data[0]->nama,
                    "telepon" => $data[0]->telepon,
                    "jenis_kelamin" => $data[0]->jenis_kelamin,
                    "tanggal" =>  $data[0]->tanggal,
                    "jam_bertemu" => $data[0]->jam_bertemu,
                    "jam_masuk" => $data[0]->jam_masuk,
                    "jam_keluar" => $data[0]->jam_keluar,
                    "asal_instansi" => $data[0]->asal_instansi,
                    "keperluan" => $data[0]->keperluan,
                    "foto_berkas" => $data[0]->foto_berkas,
                    "foto_identitas" => $data[0]->foto_identitas,
                    "status" => $data[0]->status_tamu
                ];
                $query = $this->db->insert("tb_riwayat_tamu", $tamu);*/
                redirect('admin/tamu');
            else:
                redirect('admin/tamu?error=riwayat gagal ditambah');
            endif;
        }else {
            $msg = "Terjadi Kesalahan";
            $this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
            redirect('admin/tamu?error='.$msg, 'refresh');
        }
    }

    public function uploadBerkas()
    {

		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|doc|docx|pdf';
		$config['encrypt_name']         = true;
		$config['max_size']             = '10000'; //kilobyte
		$this->load->library('upload', $config);

		
		$data = $this->input->post();

		$foto_berkas = "";

        if(!empty($_FILES['foto_berkas']['name'])):
            if ( ! $this->upload->do_upload('foto_berkas'))
            {
                    $err[] = $this->upload->display_errors();
            }
            else
            {
                    $_data = array('upload_data' => $this->upload->data());
                    $foto_berkas = $_data['upload_data']['file_name'];
            }
        else:
            $err[] = "File tidak ditemukan";
        endif;

		if(true) {
			$id = $data['id_visitation'];
            unset($data['id_visitation']);
            unset($data['foto_berkas']);
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				$data = array_merge($data, ["foto_berkas" => $foto_berkas]);
				$this->m_tamu->update($id, $data);
				$msg = "Berkas Berhasil Ditambah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('admin/tamu', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('admin/tamu', 'refresh');
            }
            echo $msg;
		}else {
			$msg = "Data tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('admin/anggota', 'refresh');
		}

    }

    public function riwayat()
    {
        $head['judul']='Riwayat Tamu';
        
        $this->load->view('templates/header',$head);
        $this->load->view('templates/sidebar',$head);

        
        switch($this->session->userdata('role')):
            case 'admin':
                $data['daftar_tamu'] = $this->M_tamu->getRiwayatAll();
                
            break;
            case 'pimpinan':
                
                $data['tamu'] = $this->M_tamu->getRiwayatByKepala($this->session->userdata('id_user'));
            break;
            default:
                $data['daftar_tamu'] = "";
                $data['tamu'] = "";
            break;
        endswitch;

        $this->load->view('admin/riwayat', $data);
        $this->load->view('templates/footer');
    }

    public function laporan()
    {
        $head['judul']='Laporan';
        
        $this->load->view('templates/header',$head);
        $this->load->view('templates/sidebar',$head);
        $this->load->view('admin/laporan');
        $this->load->view('templates/footer');
    }

    public function printLaporan()
    {
        $data['daftar_tamu'] = $this->M_tamu->getListByMonthAndYear($this->input->get('bulan'),$this->input->get('tahun'));
		$html = $this->load->view('report/laporanDaftarTamu',$data, true);

		$mpdf = new \Mpdf\Mpdf([
			'mode' => 'utf-8',
			'format' => 'A4', //F4
			'orientation' => 'L',
			'margin_left' => 2,
			'margin_right' => 2,
			'margin_top' => 5,
			'margin_bottom' => 5,
			'tempDir' => 'files'
			]);
		$mpdf->AddPage('P');
		$mpdf->WriteHTML($html);
		if($save)
		{
			$mpdf->Output('./report.pdf','F');
		}else {
			$mpdf->Output();
		}
    }

    public function ubah_sandi()
    {
        $head['judul']='Ubah Kata Sandi';
        
        $this->load->view('templates/header',$head);
        $this->load->view('templates/sidebar',$head);
        $this->load->view('admin/ubahsandi');
        $this->load->view('templates/footer');
    }

    public function ubahSandi()
    {
        $data = $this->input->post();
		if($this->input->post('old_password') != '') {
            $id = $this->session->userdata('id_user');
            unset($data['id_user']);
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
            }
            
            if($data['new_password'] != $data['new_password'])
            {
                $err[] = "Konfirmasi sandi tidak sama";
            }

			if(!isset($err))
			{
				$this->db->where("id_user", $id);
                $query = $this->db->update("tb_user", ["password" => $data['new_password']]);

                if($query) {
                    $msg = "Kata Sandi berhasil diubah";
                    $this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
                    redirect('admin/ubah_sandi');
                } else {
                    $msg = "Kesalahan saat memproses";
                    $this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
                    redirect('admin/ubah_sandi?id='.$data['id_user'].'&error='.$msg, 'refresh');
                    redirect('admin/ubah_sandi?id='.$data['id_user'].'&error=Kesalahan saat menambah user');
                }
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('admin/ubah_sandi?id='.$id.'&error='.$msg, 'refresh');
			}
		}else {
			$msg = "Data tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('admin/ubah_sandi?id='.$this->input->post('id').'&error='.$msg, 'refresh');
		}
    }
}
