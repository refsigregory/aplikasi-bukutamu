<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<style>
    body {
        font-family: times new roman;
    }
    .header {
        border-bottom: 4px groove black;
    }
	body {
		margin: 0;
        font-size: 10pt;
	}

    .border {
        border: groove 2px black;
        padding: 5px;
    }

	h1 {
		font-size: 10pt;
		text-align:center;
        margin:0;
        padding:0;
	}
	
    table {
		text-align:center;
		width:100%
    }

	td {
		padding: 5px;
		text-align: center;
	}
    img {
        width:100px;
    }
</style>
</head>
<body>
<div clas="header">
<table>
<tr>
<td><div style=""><img src="<?=base_url('/assets/img/sulut.png');?>" /></div></td>
<td style="text-align:center">
<h1 style="font-size:10pt;font-weight:none;">PEMERINTAH PROVINSI SULAWESI UTARA</h1>
<h1 style="font-size:14pt">DINAS KOMUNIKASI, INFORMATIKA, PERSANDIAN DAN<br>STATISTIK DAERAH PROVINSI SULAWEESI UTARA</h1>
Jl. 17 Agustus No.69 Telp.(0431) 865559 Fax (0431) 865471 Manado 95119<br>
Website : https://sulutprov.go.id E-mail: dkips@sulutprov.go.id
</td>
</tr>
</table>
</div>
<div class="header"></div>
<br></br>
<?php
function dateCodeToText($date){
    $date = str_replace('Jan', 'Januari', $date);
    $date = str_replace('Feb', 'Februari', $date);
    $date = str_replace('Mar', 'Maret', $date);
    $date = str_replace('Apr', 'April', $date);
    $date = str_replace('May', 'Mei', $date);
    $date = str_replace('Jun', 'Juni', $date);
    $date = str_replace('Jul', 'Juli', $date);
    $date = str_replace('Aug', 'Agustus', $date);
    $date = str_replace('Sep', 'September', $date);
    $date = str_replace('Oct', 'Oktober', $date);
    $date = str_replace('Nov', 'November', $date);
    $date = str_replace('Dec', 'Desember', $date);
    return $date;
}
?>
<h1>Daftar Tamu <?=dateCodeToText(date("M Y", strtotime(date($_GET['tahun'] . "-" . $_GET['bulan']))));?></h1>
<br>
</br>
<div>
                <table class="table table-bordered" border="1" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama Tamu</th>
                      <th>Jam Bertemu</th>
                      <th>Asal Instansi</th>
                      <th>Bertemu Dengan</th>
                      <th>Jenis Keperluan</th>
                      <th>Keperluan</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; if($daftar_tamu != ""): foreach($daftar_tamu as $row):?>
                    <tr>
                      <td><?=$no;?></td>
                      <td><?=$row->nama;?></td>
                      <td><?=$row->jam_masuk;?> <?=($row->jam_keluar != '' && $row->jam_keluar != '00:00:00') ? ' - ' . $row->jam_keluar : '';?></td>
                      <td><?=$row->asal_instansi;?></td>
                      <td><?=$this->m_pengguna->getByID($row->id_user_pimpinan)[0]->nama;?></td>
                      <td><?=$this->m_tamu->getKeperluanByID($row->id_typeofneeds)[0]->jenis_keperluan;?></td>
                      <td><?=$row->keterangan;?></td>
                    </tr>
                    <?php $no++; endforeach; endif;?>
                  </tbody>
                </table>
</div>
</body>
</html>