

<body class="bg-gradient-grey">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-5 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">

                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">PENDAFTARAN</h1>
                  </div>
  <p class="login-box-msg">
      <?php
          if (!empty($this->session->flashdata('msg'))):
              $msg = $this->session->flashdata('msg');
      ?>
      <?php if($msg['type'] == 'success'): ?>
          <div class="alert alert-success"><?=$msg['message'];?></div>
      <?php elseif ($msg['type'] == 'warning'): ?>
          <div class="alert alert-warning"><?=$msg['message'];?></div>
      <?php elseif ($msg['type'] == 'error'): ?>
          <div class="alert alert-danger"><?=$msg['message'];?></div>
      <?php else: ?>
          <div class="alert alert-info"><?=$msg['message'];?></div>
      <?php endif; ?>
      <?php endif; ?>
  </p>
                  <form action="<?= site_url('auth/process_register') ?>" class="user" method="POST">
                    <div class="form-group">
                      <input type="email" class="form-control form-control-user" id=""  name="email" placeholder="email" name="email" required>
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" id="" name="nama" placeholder="nama" name="nama" required>
                    </div>
                    <div class="form-group">
                        <select name="jenis_kelamin" type="text" class="form-control form-control-user">
                            <option> - Jenis Kelamin - </option>
                            <option>Laki-laki</option>
                            <option>Perempuan</option>
                        </select>
                    </div>

                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" id="" name="alamat" placeholder="alamat" name="alamat" required>
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" id="" name="asal_instansi" placeholder="asal instansi" name="asal_instansi" required>
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" id="" name="telepon" placeholder="telepon" name="telepon" required>
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" id="" name="username" placeholder="username" name="username" required>
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" id="" name="password" placeholder="password" name="password" required>
                    </div>
                    <div class="form-group">
                    </div>
                    <button class="btn btn-primary btn-user btn-block" type="submit">
                      Mendaftar
                    </button>
                    <div class="text-center"><a href="<?=base_url('auth/login');?>">
                      Kembali ke Login
                    </a></div>
                  </form>
                  <hr>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

