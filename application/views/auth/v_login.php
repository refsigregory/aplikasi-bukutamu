

<body class="bg-gradient-grey">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-5 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                
      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= site_url('auth')?>">
        <div class="sidebar-brand-icon">
          <img class="img-profile rounded-circle" style="width : 80px" src="<?= base_url()  ?>/assets/img/logo.png">
        </div>
      </a>
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">KOMINFO SULUT</h1>
                  </div>
<p class="login-box-msg">
<?php
if (!empty($this->session->flashdata('msg'))):
$msg = $this->session->flashdata('msg');
?>
<?php if($msg['type'] == 'success'): ?>
<div class="alert alert-success"><?=$msg['message'];?></div>
<?php elseif ($msg['type'] == 'warning'): ?>
<div class="alert alert-warning"><?=$msg['message'];?></div>
<?php elseif ($msg['type'] == 'error'): ?>
<div class="alert alert-danger"><?=$msg['message'];?></div>
<?php else: ?>
<div class="alert alert-info"><?=$msg['message'];?></div>
<?php endif; ?>
<?php endif; ?>
</p>
                  <form action="<?= site_url('auth/process') ?>" class="user" method="POST">
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" id="" aria-describedby="emailHelp" placeholder="username" name="username" required>
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" id="" placeholder="Password" name="password" required>
                    </div>
                    <div class="form-group">
                    </div>
                    <button class="btn btn-primary btn-user btn-block" type="submit" name="login">
                      Login
                    </button>
                    <a href="<?=base_url('auth/register');?>" class="btn btn-danger btn-user btn-block">
                      Register
                    </a>
                  </form>
                  <hr>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

