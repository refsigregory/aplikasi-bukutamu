
<style>
#my_camera {
    min-width: 490;
    min-height: 390;
    border: solid 2px black;
}
</style>
<body class="bg-gradient-grey">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-9 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-3">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">No-Appointment</h1>
                  </div>
                  <p class="login-box-msg">
      <?php
          if (!empty($this->session->flashdata('msg'))):
              $msg = $this->session->flashdata('msg');
      ?>
      <?php if($msg['type'] == 'success'): ?>
          <div class="alert alert-success"><?=$msg['message'];?></div>
      <?php elseif ($msg['type'] == 'warning'): ?>
          <div class="alert alert-warning"><?=$msg['message'];?></div>
      <?php elseif ($msg['type'] == 'error'): ?>
          <div class="alert alert-danger"><?=$msg['message'];?></div>
      <?php else: ?>
          <div class="alert alert-info"><?=$msg['message'];?></div>
      <?php endif; ?>
      <?php endif; ?>
  </p>
                  <form method="POST" action="<?=base_url('auth/newTamu');?>">

        <div class="row">

            <div class="col-md">

            <div class="form-group">
                <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap">
            </div>

            <div class="form-group">
                <select type="text" class="form-control" name="id_user_pimpinan">
                    <option> - Pilih Pimpinan - </option>
                    <?php if($pimpinan != ""):?>
                        <?php foreach($pimpinan as $row):?>
                          <option value="<?=$row->id_user;?>"><?=$row->nama;?> - <?=$row->status_keberadaan;?></option>
                        <?php endforeach;?>
                    <?php endif;?>
                </select>
            </div>


            <div class="form-group">
                <select name="jenis_kelamin" type="text" class="form-control">
                    <option> - Jenis Kelamin - </option>
                    <option>Laki-laki</option>
                    <option>Perempuan</option>
                </select>
            </div>

            <div class="form-group">
                <input type="text" class="form-control" name="tanggal" value="<?=date("Y-m-d");?>" placeholder="Tanggal" readonly="">
            </div>


            <div class="form-group">
                <input type="text" class="form-control" name="asal_instansi" placeholder="Asal Instansi">
            </div>

            <div class="form-group">
                <select type="text" class="form-control" name="id_typeofneeds">
                    <option> - Pilih Keperluan - </option>
                    <?php if($keperluan != ""):?>
                        <?php foreach($keperluan as $row):?>
                          <option value="<?=$row->id_typeofneeds;?>"><?=$row->jenis_keperluan;?></option>
                        <?php endforeach;?>
                    <?php endif;?>
                </select>
            </div>

            <div class="form-group">
                <textarea class="form-control" name="keterangan" placeholder="Keterangan"></textarea>
            </div>

            </div>

            <div class="col-md">
                <div id="my_camera"></div>
                <br/>
                <input type=button value="Ambil" onClick="take_snapshot()">
                <input type=button value="Ulang" onClick="reset_camera()">
                <input type="hidden" name="image" class="image-tag">
                <br/>
            </div>
            
            </div>
            
        </div>

        <div class="row">
            <div class="col-md text-center">

                <br/>

                <button class="btn btn-success">Submit</button>

            </div>
        </div>

    </form>

                 
                  <hr>
                </div>

              
              </div>
            </div>
          </div>
        </div>

        <div class="col-xl-3 col-lg-12 col-md-9">

<div class="card o-hidden border-0 shadow-lg my-5">
  <div class="card-body p-0">
    <!-- Nested Row within Card Body -->
    <div class="row">
      <div class="col-lg-12">
        <div class="p-2">
        <div class="col-lg-12" style="font-size: 8pt">
        <h1 class="h4 text-gray-900 mb-4">Daftar Tamu</h1>
                <table class="table table-bordered" id="" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Jam</th>
                      <th>Nama Tamu</th>
                      <th>Bertemu Dengan</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; if($daftar_tamu != ""): foreach($daftar_tamu as $row):?>
                      <td><?=date("H:i", strtotime($row->jam_masuk));?></td>
                      <td><?=$row->nama?></td>
                      <td><?=$this->m_pengguna->getByID($row->id_user_pimpinan)[0]->nama;?></td>
                    </tr>
                    <?php $no++; endforeach; endif;?>
                  </tbody>
                </table>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>


      </div>
      <a href="<?=base_url('auth/logout');?>">Keluar</a>
    </div>

  </div>

<script src="<?= base_url();?>/assets/vendor/webcamjs/webcam.min.js"></script>
<script language="JavaScript">

function start_camera() {
  Webcam.set({

      width: 490,

      height: 390,

      image_format: 'jpeg',

      jpeg_quality: 90

  });
  Webcam.attach( '#my_camera' );
}

start_camera();

function take_snapshot() {

    Webcam.snap( function(data_uri) {

        $(".image-tag").val(data_uri);

        // document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
        Webcam.reset( '#my_camera' );
        document.getElementById('my_camera').innerHTML = '<img src="'+data_uri+'"/>';

    } );

}

function reset_camera()
{
  document.getElementById('my_camera').innerHTML = '';
  start_camera();
}

</script>


