
  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-danger sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon ">
          <img class="" style="width : 60px" src="<?= base_url()  ?>/assets/img/sulut.png">
        </div>
        <div class="sidebar-brand-text mx-3">KOMINFO SULUT</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="<?= site_url('admin')?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <?php if($this->session->userdata('role') == 'admin'):?>
      <!-- Heading -->
      <div class="sidebar-heading">
        Master Data
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?= site_url('admin/pengguna')?>">
          <i class="fas fa-fw fa-user"></i>
          <span>Pengguna</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        </div>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">
      <?php endif;?>

      <?php if($this->session->userdata('role') == 'admin'):?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
        <i class="fas fa-fw fa-plus" ></i>
          <span>Tambah Tamu</span></a>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="<?= site_url('admin/new_appointment')?>">Appointment</a>
            <a class="collapse-item" href="<?= site_url('admin/tambah_tamu')?>">No-Appointment</a>
          </div>
        </div>
      </li>
      <?php endif;?>


      <?php if($this->session->userdata('role') == 'pengguna'):?>
      <li class="nav-item">
        <a class="nav-link" href="<?= site_url('admin/new_appointment')?>">
          <i class="fas fa-fw fa-plus" ></i>
          <span>Buat Appointment</span></a>
      </li>
      <?php endif;?>

      <?php if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'pimpinan' || $this->session->userdata('role') == 'pengguna'):?>
      <li class="nav-item">
        <a class="nav-link" href="<?= site_url('admin/appointment')?>">
          <i class="fas fa-fw fa-calendar" ></i>
          <span>Daftar Appointment</span></a>
      </li>
      <?php endif;?>

      <?php if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'pimpinan'):?>
      <li class="nav-item">
        <a class="nav-link" href="<?= site_url('admin/tamu')?>">
          <i class="fas fa-fw fa-users" ></i>
          <span>Daftar Tamu</span></a>
      </li>
      <?php endif;?>
      
      <?php if($this->session->userdata('role') == 'admin'):?>
      <li class="nav-item">
        <a class="nav-link" href="<?= site_url('admin/riwayat')?>">
          <i class="fas fa-fw fa-file" ></i>
          <span>Riwayat Tamu</span></a>
      </li>
      <?php endif;?>

      <?php if($this->session->userdata('role') == 'admin'):?>
      <!-- Nav Item - Charts -->
      <li class="nav-item">
        <a class="nav-link" href="<?= site_url('admin/laporan')?>">
          <i class="fas fa-fw fa-file"></i>
          <span>Laporan</span></a>
      </li>
      <?php endif;?>


      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">
    </ul>
    <!-- End of Sidebar -->



    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">



          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Alerts -->
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="<?=base_url();?>" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <!-- Counter - Alerts -->
                <span class="badge badge-danger badge-counter"><?=$this->m_pengguna->getCountNotifikasiByIDUser($this->session->userdata('id'));?></span>
              </a>
              </li>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?=$this->session->userdata("name");?> (<?=$this->session->userdata("role");?>)</span>
                <img class="img-profile rounded-circle" src="<?=base_url('assets/img/user.png');?>">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="<?= site_url('admin/ubah_sandi')?>">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Ubah Kata Sandi
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->
