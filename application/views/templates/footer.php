
      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Teknik Informatika &copy; 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"></h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Apakah anda yakin ?</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Tidak</button>
          <a class="btn btn-primary" href="<?= site_url('auth/logout')?>">Ya</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url()  ?>/assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url()  ?>/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url()  ?>/assets/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url()  ?>/assets/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="<?= base_url()  ?>/assets/vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script>
    // Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

function number_format(number, decimals, dec_point, thousands_sep) {
  // *     example: number_format(1234.56, 2, ',', ' ');
  // *     return: '1 234,56'
  number = (number + '').replace(',', '').replace(' ', '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + Math.round(n * k) / k;
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}
<?php
  $query = $this->db->query("SELECT tanggal as bulan, count(*) as jumlah FROM `tb_visitation` group by month(tanggal)");
?>
// Area Chart Example
var ctx = document.getElementById("myAreaChart");
var myLineChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: [<?php foreach ($query->result() as $d): echo '"'. date("M", strtotime("$d->bulan")) . '",'; endforeach; ?>],
    datasets: [{
      label: "Tamu",
      lineTension: 0.3,
      backgroundColor: "rgba(78, 115, 223, 0.05)",
      borderColor: "rgba(78, 115, 223, 1)",
      pointRadius: 3,
      pointBackgroundColor: "rgba(78, 115, 223, 1)",
      pointBorderColor: "rgba(78, 115, 223, 1)",
      pointHoverRadius: 3,
      pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
      pointHoverBorderColor: "rgba(78, 115, 223, 1)",
      pointHitRadius: 10,
      pointBorderWidth: 2,
      data: [<?php foreach ($query->result() as $d): echo ''. $d->jumlah . ','; endforeach; ?>],
    }],
  },
  options: {
    maintainAspectRatio: false,
    layout: {
      padding: {
        left: 10,
        right: 25,
        top: 25,
        bottom: 0
      }
    },
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
        },
        gridLines: {
          display: false,
          drawBorder: false
        },
        ticks: {
          maxTicksLimit: 7
        }
      }],
      yAxes: [{
        ticks: {
          maxTicksLimit: 5,
          beginAtZero: true,
          padding: 10,
          // Include a dollar sign in the ticks
          callback: function(value, index, values) {
            return value + ' orang';
          }
        },
        gridLines: {
          color: "rgb(234, 236, 244)",
          zeroLineColor: "rgb(234, 236, 244)",
          drawBorder: false,
          borderDash: [2],
          zeroLineBorderDash: [2]
        }
      }],
    },
    legend: {
      display: false
    },
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      intersect: false,
      mode: 'index',
      caretPadding: 10,
      callbacks: {
        label: function(tooltipItem, chart) {
          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
          return datasetLabel + ': ' + tooltipItem.yLabel + ' orang';
        }
      }
    }
  }
});

 </script>
<?php
  $query = $this->db->query("SELECT year(tanggal) as tahun, count(*) as jumlah FROM `tb_visitation` group by year(tanggal)");
?>
  <script>
  // Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

// Pie Chart Example
var ctx = document.getElementById("myPieChart");
var myPieChart = new Chart(ctx, {
  type: 'doughnut',
  data: {
    labels: [<?php foreach ($query->result() as $d): echo ''. $d->tahun . ','; endforeach; ?>],
    datasets: [{
      data: [<?php foreach ($query->result() as $d): echo ''. $d->jumlah . ','; endforeach; ?>],
      backgroundColor: ['#4e73df', '#1cc88a', '#36b9cc'],
      hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf'],
      hoverBorderColor: "rgba(234, 236, 244, 1)",
    }],
  },
  options: {
    maintainAspectRatio: false,
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      caretPadding: 10,
    },
    legend: {
      display: false
    },
    cutoutPercentage: 80,
  },
});

  </script>

  <script src="<?= base_url();?>/assets/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= base_url();?>/assets/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <script src="<?= base_url();?>/assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

  <script type="text/javascript">
  Date.prototype.addDays = function(days) {
      this.setDate(this.getDate() + parseInt(days));
      return this;
  };
  
    $('#datepicker').datepicker({
      startDate : new Date(),
        todayHighlight : true
    });
    $('#datepicker').datepicker("setDate", new Date());

    var dateNow =  new Date();
    $('#datepicker_appointment').datepicker({
      startDate : dateNow.addDays(1),
        todayHighlight : true
    });
    $('#datepicker_appointment').datepicker("setDate", dateNow);
</script>

  <script>
  $(function () {
    $('#show-modal').modal('show');

    $('#showlist-tamu').modal('show');

    setInterval(() => {
      $('#showlist-tamu').modal('hide');
    }, 5000);

    $('#dataTable').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true,
      "language": {
                    "sEmptyTable":   "Tidak ada data yang tersedia pada tabel ini",
                    "sProcessing":   "Sedang memproses...",
                    "sLengthMenu":   "Tampilkan _MENU_ entri",
                    "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                    "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                    "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
                    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                    "sInfoPostFix":  "",
                    "sSearch":       "Cari:",
                    "sUrl":          "",
                    "oPaginate": {
                        "sFirst":    "Pertama",
                        "sPrevious": "Sebelumnya",
                        "sNext":     "Selanjutnya",
                        "sLast":     "Terakhir"
                    }
                }
    });

    $("#tambahPeran").change(() =>
    {
      var role = $("#tambahPeran").val();
      var formPengguna = '<div class="form-group"><label>Nama</label><input type="text" class="form-control" name="nama" placeholder=""></div>';
      var formAdmin = '<div class="form-group"><label>Nama</label><input type="text" class="form-control" name="nama" placeholder=""></div>';
      var formKepala = '<div class="form-group"><label>Nama</label><input type="text" class="form-control" name="nama" placeholder=""></div>';
      var formTamu = '';

      formPengguna += '<div class="form-group"><select name="jenis_kelamin" type="text" class="form-control"><option> - Jenis Kelamin - </option><option>Laki-laki</option><option>Perempuan</option></select></div>';
      formPengguna += '<div class="form-group"><label>Alamat</label><input type="text" name="alamat" class="form-control" placeholder=""></div>';
      formPengguna += '<div class="form-group"><label>Asal Instansi</label><input type="text" name="asal_instansi" class="form-control" placeholder=""></div>';
      formPengguna += '<div class="form-group"><label>Email</label><input type="email" name="email" class="form-control" placeholder=""></div>';
      formPengguna += '<div class="form-group"><label>Telepon</label><input type="text" name="telepon" class="form-control" placeholder=""></div>';

      formKepala+= '<div class="form-group"><label>Jabatan</label><select name="id_job" class="form-control"><option value=""> - Pilih Jabatan - </option><option value="1">Kepala Dinas</option><option value="3">Sektretaris</option><option value="2">Kepala Bidang</option></select></div>';
      

      switch(role) {
        case '1':
          $("#formTambah").html(formAdmin);
          break;
        case '3':
          $("#formTambah").html(formPengguna);
          break;
        case '4':
          $("#formTambah").html(formKepala);
          break;
        case '2':
          $("#formTambah").html(formTamu);
          break;
        default:
          $("#formTambah").html('');
          break;
      }
      
    })
  })

  function Tanya()
  {
      var tanya = confirm("Anda yakin akan melakukan aksi ini?");
      if(tanya == true)
          {
              return true;
          }else {
              return false;
          }
  }
  </script>

<audio id="notifsound">
  <source src="<?=base_url('assets/sound/notif.mp3');?>" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>

<script>
  //$('#notifsound')[0].play();
</script>

<?php if($this->router->fetch_method()=='tamu'):?>
  <script>
    var i = 0;
    setInterval(function(){ // untuk cek jika ada data baru
      console.log("updating");
      var jumlah = 0;
      jumlah = <?php
      switch($this->session->userdata('role')):
            case 'admin':
                echo $this->m_tamu->getCountListTodayAll();
            break;

            case 'pimpinan':
                echo $this->m_tamu->getCountListTodayByKepala($this->session->userdata('id_user'));
            break;

            default:
                echo 0;
            break;
        endswitch;
        ?>;
      console.log("Tamu Sekarang: " +jumlah);
      $.ajax({
        method: "POST",
        url: "<?=base_url('auth/checkDataTamu?role='.$this->session->userdata("role").'&id='.$this->session->userdata('id_user'));?>",
      })
        .done(function( msg ) {
          console.log("Tamu Baru: " +msg);
          if(msg > jumlah)
          {
            //console.log("Updated!");
            $('#notifsound')[0].play();
            location = ""; //refresh
          }
        });
    }, 5000);
  </script>
<?php endif;?>

<script>
    var i = 0;
    setInterval(function(){ // untuk cek jika ada data baru
      var jumlah = 0;
      jumlah = <?=$this->m_pengguna->getCountNotifikasiByIDUser($this->session->userdata('id'));?>;
      $.ajax({
        method: "POST",
        url: "<?=base_url('auth/checkNotif?id='.$this->session->userdata('id'));?>",
      })
        .done(function( msg ) {
          if(msg > jumlah)
          {
            $('#notifsound')[0].play();
            location = ""; //refresh
          }
        });
    }, 5000);
  </script>
</body>

</html>