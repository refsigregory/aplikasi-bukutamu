<!-- Begin Page Content -->
<div class="container-fluid">
<?php if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'pimpinan'):?>
<?php if($tamu != ""):?>
  <!-- Card -->
  <div class="card mb-3" style="max-width: 100%;" id="visitor">
  <div class="row no-gutters">
    <div class="col-md-3">
      <img src="<?=base_url('uploads/') . $tamu[0]->foto_identitas;?>" class="card-img" alt="<?=$tamu[0]->nama;?>">
    </div>
    <div class="col-md-9">
      <div class="card-body">
        <h5 class="card-title"><?=$tamu[0]->nama;?></h5>
        <p class="card-text"><?=$tamu[0]->keterangan;?></p>
        <p class="card-text"><small class="text-muted"><?=$tamu[0]->tanggal;?> <?=$tamu[0]->jam_masuk;?></small></p>
        <p class="card-text">
          <a href="<?=base_url('admin/selesaiTamu?id='.$tamu[0]->id_visitation);?>">
          <button class="btn btn-sm btn-success" title="Selesai"><i class="fa fa-check"></i></button>
          </a>

          <a href="<?=base_url('admin/tamu?berkas='.$tamu[0]->id_visitation);?>">
          <button class="btn btn-sm btn-warning" title="Upload Berkas"><i class="fa fa-file"></i></button>
          </a>

          <a href="<?=base_url('admin/cancelTamu?id='.$tamu[0]->id_visitation);?>">
          <button class="btn btn-sm btn-danger" title="Batal"><i class="fa fa-window-close"></i></button>
          </a>
        </p>
      </div>
    </div>
  </div>
</div>
<?php endif;?>
<?php endif;?>

  <!-- Page Heading -->
  <section class="content-header">
    <h1 class="h3 mb-4 text-gray-800">Daftar Tamu</h1>
  </section>
  <section>
  <p class="login-box-msg">
      <?php
          if (!empty($this->session->flashdata('msg'))):
              $msg = $this->session->flashdata('msg');
      ?>
      <?php if($msg['type'] == 'success'): ?>
          <div class="alert alert-success"><?=$msg['message'];?></div>
      <?php elseif ($msg['type'] == 'warning'): ?>
          <div class="alert alert-warning"><?=$msg['message'];?></div>
      <?php elseif ($msg['type'] == 'error'): ?>
          <div class="alert alert-danger"><?=$msg['message'];?></div>
      <?php else: ?>
          <div class="alert alert-info"><?=$msg['message'];?></div>
      <?php endif; ?>
      <?php endif; ?>
  </p>       
          <!-- DataTales -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary float-left">Tamu</h6>
              <form method="post" action="<?=base_url('admin/setKeberadaan');?>">
              <div class="float-right">
                    <button type="submit" class="btn btn-primary" >
                        SET
                    </button>
              </div>
              <div class="float-right">
                    <select type="text" class="form-control" name="status_keberadaan">
                        <option>ada</option>
                        <option>keluar</option>
                        <option>sibuk</option>
                    </select>
              </div>
              <div class="float-right">
              <?php if($this->session->userdata("role") == "admin"):?>
    <div class="form-group">
                    <select type="text" class="form-control" name="id_user_pimpinan">
                        <option> - Pilih Pimpinan - </option>
                        <?php if($pimpinan != ""):?>
                            <?php foreach($pimpinan as $row):?>
                              <option value="<?=$row->id_user;?>"><?=$row->nama;?> - <?=$row->status_keberadaan;?></option>
                            <?php endforeach;?>
                        <?php endif;?>
                    </select>
    </div>
    <?php else:?>
        <input type="hidden" name="id_user_pimpinan" value="<?=$this->session->userdata("id_user");?>">
    <?php endif;?>
              </div>
              </form>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Tanggal</th>
                      <th>Nama Tamu</th>
                      <th>Jam Bertemu</th>
                      <th>Asal Instansi</th>
                      <th>Bertemu Dengan</th>
                      <th>Jenis Keperluan</th>
                      <th>Keperluan</th>
                      <th>Status</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; if($daftar_tamu != ""): foreach($daftar_tamu as $row):?>
                      <td><?=$no;?></td>
                      <td><?=$row->tanggal;?></td>
                      <td><?=$row->nama;?></td>
                      <td><?=$row->jam_masuk;?> <?=($row->jam_keluar != '' && $row->jam_keluar != '00:00:00') ? ' - ' . $row->jam_keluar : '';?></td>
                      <td><?=$row->asal_instansi;?></td>
                      <td><?=$this->m_pengguna->getByID($row->id_user_pimpinan)[0]->nama;?></td>
                      <td><?=$this->m_tamu->getKeperluanByID($row->id_typeofneeds)[0]->jenis_keperluan;?></td>
                      <td><?=$row->keterangan;?></td>
                      <td><?=$row->status_respon;?></td>
                      <td>
                            <a href="<?=base_url('admin/tamu?show='.$row->id_visitation);?>">
                            <button class="btn btn-sm btn-info" title="Lihat"><i class="fa fa-eye"></i></button>
                            </a>
                            <?php if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'kepala'):?>
                            <a href="<?=base_url('admin/selesaiTamu?id='.$row->id_visitation);?>">
                            <button class="btn btn-sm btn-success" title="Selesai"><i class="fa fa-check"></i></button>
                            </a>

                            <a href="<?=base_url('admin/tamu?berkas='.$row->id_visitation);?>">
                            <button class="btn btn-sm btn-warning" title="Upload Berkas"><i class="fa fa-file"></i></button>
                            </a>
                            
                            <a href="<?=base_url('admin/cancelTamu?id='.$row->id_visitation);?>">
                            <button class="btn btn-sm btn-danger" title="Batal"><i class="fa fa-window-close"></i></button>
                            </a>
                            <?php endif;?>
                            
                      </td>
                    </tr>
                    <?php $no++; endforeach; endif;?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
   </section>
</div>