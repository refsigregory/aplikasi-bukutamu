<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <section class="content-header">
    <h1 class="h3 mb-4 text-gray-800">Pengguna</h1>
  </section>
  <p class="login-box-msg">
      <?php
          if (!empty($this->session->flashdata('msg'))):
              $msg = $this->session->flashdata('msg');
      ?>
      <?php if($msg['type'] == 'success'): ?>
          <div class="alert alert-success"><?=$msg['message'];?></div>
      <?php elseif ($msg['type'] == 'warning'): ?>
          <div class="alert alert-warning"><?=$msg['message'];?></div>
      <?php elseif ($msg['type'] == 'error'): ?>
          <div class="alert alert-danger"><?=$msg['message'];?></div>
      <?php else: ?>
          <div class="alert alert-info"><?=$msg['message'];?></div>
      <?php endif; ?>
      <?php endif; ?>
  </p>
  <section>
          <!-- DataTales -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary float-left">Pengguna</h6>
              <a href="<?=base_url('admin/tambah_pengguna');?>" class="btn btn-success float-right">Tambah</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Username</th>
                      <th>Peran</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if($pengguna != ""): foreach($pengguna as $row):?>
                      <td><?=$row->username;?></td>
                      <td><?=$this->m_pengguna->getUsertypeByID($row->id_usertype);?></td>
                      <td>
                          <a href="?detail=<?=$row->id_user;?>" class="btn btn-sm btn-info">
                            <i class="fa fa-eye"></i>
                          </a>

                          <a href="<?=base_url('admin/edit_pengguna/');?>?id=<?=$row->id_user;?>" class="btn btn-sm btn-warning">
                            <i class="fa fa-pen"></i>
                          </a>

                          <a href="<?=base_url('admin/hapus_pengguna/');?><?=$row->id_user;?>" class="btn btn-sm btn-danger" onclick="return Tanya();">
                            <i class="fa fa-trash"></i>
                          </a>
                      </td>
                    </tr>
                    <?php endforeach; endif;?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
   </section>
</div>