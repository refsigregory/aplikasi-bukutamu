<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <section class="content-header">
    <h1 class="h3 mb-4 text-gray-800">Tambah Pengguna</h1>
  </section>
  <section>
  <p class="login-box-msg">
      <?php
          if (!empty($this->session->flashdata('msg'))):
              $msg = $this->session->flashdata('msg');
      ?>
      <?php if($msg['type'] == 'success'): ?>
          <div class="alert alert-success"><?=$msg['message'];?></div>
      <?php elseif ($msg['type'] == 'warning'): ?>
          <div class="alert alert-warning"><?=$msg['message'];?></div>
      <?php elseif ($msg['type'] == 'error'): ?>
          <div class="alert alert-danger"><?=$msg['message'];?></div>
      <?php else: ?>
          <div class="alert alert-info"><?=$msg['message'];?></div>
      <?php endif; ?>
      <?php endif; ?>
  </p>
  <form method="post" action="<?=base_url('admin/tambahPengguna');?>">
  <div class="form-group">
    <label>Username</label>
    <input type="text" class="form-control" name="username" placeholder="">
  </div>
  <div class="form-group">
    <label>Password</label>
    <input type="password" class="form-control" name="password" placeholder="">
  </div>
  <div class="form-group">
    <label>Peran</label>
    <select class="form-control" name="id_usertype" id="tambahPeran">
      <option value=""> - Pilih Peran - </option>
      <option value="1">Admin</option>
      <option value="4">Kepala Dinas/ Sekretaris / Kepala Bidang</option>
      <option value="3">Tamu Terdaftar</option>
      <option value="2">Tamu</option>
    </select>
  </div>

  <div id="formTambah">
  </div>
  <button type="submit" class="btn btn-primary btn-block">Tambah</button>
</form>
  </section>
</div>