
  <div class="modal fade" id="show-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><?=$detail->nama;?></h4>
      </div>
      
      <div class="modal-body">
          <div class="row">
            <div class="col-xs"><b>Nama</b></div>
            <div class="col-xs">&nbsp;:&nbsp;</div>
            <div class="col-xs"><?=$detail->nama;?></div>
          </div>
      </div>

      <div class="modal-body">
          <div class="row">
            <div class="col-xs"><b>Alamat</b></div>
            <div class="col-xs">&nbsp;:&nbsp;</div>
            <div class="col-xs"><?=$detail->alamat;?></div>
          </div>
      </div>

      <div class="modal-body">
          <div class="row">
            <div class="col-xs"><b>Email</b></div>
            <div class="col-xs">&nbsp;:&nbsp;</div>
            <div class="col-xs"><?=$detail->email;?></div>
          </div>
      </div>

      <div class="modal-body">
          <div class="row">
            <div class="col-xs"><b>Telepon</b></div>
            <div class="col-xs">&nbsp;:&nbsp;</div>
            <div class="col-xs"><?=$detail->telepon;?></div>
          </div>
      </div>

      <div class="modal-body">
          <div class="row">
            <div class="col-xs"><b>Asal Instansi</b></div>
            <div class="col-xs">&nbsp;:&nbsp;</div>
            <div class="col-xs"><?=$detail->asal_instansi;?></div>
          </div>
      </div>




      
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->