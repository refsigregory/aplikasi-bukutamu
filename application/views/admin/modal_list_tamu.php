
  <div class="modal fade" id="showlist-tamu">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Daftar Antrian Tamu</h4>
      </div>
      
      <div class="modal-body">

      <div class="table-responsive">
                <table class="table table-bordered" id="" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Tanggal</th>
                      <th>Nama Tamu</th>
                      <th>Jam Bertemu</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; if($daftar_tamu != ""): foreach($daftar_tamu as $row):?>
                      <td><?=$no;?></td>
                      <td><?=$row->tanggal;?></td>
                      <td><?=$row->nama?></td>
                      <td><?=$row->jam_masuk;?></td>
                    </tr>
                    <?php $no++; endforeach; endif;?>
                  </tbody>
                </table>
              </div>

      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->