
  <div class="modal fade" id="show-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Detail Tamu</h4>
      </div>
      
      <div class="modal-body">
      <div class="row">
      <img src="<?=base_url('uploads/') . $detail->foto_identitas;?>" class="card-img" alt="<?=$detail->nama;?>">
      </div>
          <div class="row">
            <div class="col-xs"><b>Nama</b></div>
            <div class="col-xs">&nbsp;:&nbsp;</div>
            <div class="col-xs"><?=$detail->nama;?></div>
          </div>
      </div>

      <div class="modal-body">
          <div class="row">
            <div class="col-xs"><b>Alamat</b></div>
            <div class="col-xs">&nbsp;:&nbsp;</div>
            <div class="col-xs"><?=$detail->alamat;?></div>
          </div>
      </div>

      <div class="modal-body">
          <div class="row">
            <div class="col-xs"><b>Telepon</b></div>
            <div class="col-xs">&nbsp;:&nbsp;</div>
            <div class="col-xs"><?=$detail->telepon;?></div>
          </div>
      </div>

      <div class="modal-body">
          <div class="row">
            <div class="col-xs"><b>Asal Instansi</b></div>
            <div class="col-xs">&nbsp;:&nbsp;</div>
            <div class="col-xs"><?=$detail->asal_instansi;?></div>
          </div>
      </div>

      <div class="modal-body">
          <div class="row">
            <div class="col-xs"><b>Jenis Kelamin</b></div>
            <div class="col-xs">&nbsp;:&nbsp;</div>
            <div class="col-xs"><?=$detail->jenis_kelamin;?></div>
          </div>
      </div>

      <div class="modal-body">
          <div class="row">
            <div class="col-xs"><b>Telepon</b></div>
            <div class="col-xs">&nbsp;:&nbsp;</div>
            <div class="col-xs"><?=$detail->telepon;?></div>
          </div>
        </div>

<?php if($detail->foto_berkas != ""):?>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs"><b>Berkas</b></div>
          <div class="col-xs">&nbsp;:&nbsp;</div>
          <div class="col-xs"><a href="<?=base_url();?>uploads/<?=$detail->foto_berkas;?>" target="_blank">Buka Berkas</a></div>
        </div>
      </div>
<?php endif;?>




      
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->