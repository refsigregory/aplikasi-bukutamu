<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <section class="content-header">
    <h1 class="h3 mb-4 text-gray-800">Buat Appointment</h1>
  </section>
  <section>
    <p class="login-box-msg">
      <?php
          if (!empty($this->session->flashdata('msg'))):
              $msg = $this->session->flashdata('msg');
      ?>
      <?php if($msg['type'] == 'success'): ?>
          <div class="alert alert-success"><?=$msg['message'];?></div>
      <?php elseif ($msg['type'] == 'warning'): ?>
          <div class="alert alert-warning"><?=$msg['message'];?></div>
      <?php elseif ($msg['type'] == 'error'): ?>
          <div class="alert alert-danger"><?=$msg['message'];?></div>
      <?php else: ?>
          <div class="alert alert-info"><?=$msg['message'];?></div>
      <?php endif; ?>
      <?php endif; ?>
  </p>
        <div class="row">

            <div class="col-md">

 
            <form method="POST" action="<?=base_url('admin/tambahAppointment');?>">

<div class="row">

    <div class="col-md">

    <?php if($this->session->userdata("role") == "admin"):?>
    <div class="form-group">
        <select type="text" class="form-control" name="id_user">
            <option> - Pilih Pengguna - </option>
            <?php if($pengguna != ""):?>
                <?php foreach($pengguna as $row):?>
                  <option value="<?=$row->id_user;?>"><?=$row->nama;?></option>
                <?php endforeach;?>
            <?php endif;?>
        </select>
    </div>
    <?php else:?>
        <input type="hidden" name="id_user" value="<?=$this->session->userdata("id_user");?>">
    <?php endif;?>

    <div class="form-group">
        <select type="text" class="form-control" name="id_user_pimpinan">
            <option> - Pilih Pimpinan - </option>
            <?php if($pimpinan != ""):?>
                <?php foreach($pimpinan as $row):?>
                  <option value="<?=$row->id_user;?>"><?=$row->nama;?></option>
                <?php endforeach;?>
            <?php endif;?>
        </select>
    </div>

    <div class="form-group">
        <input data-date-format="yyyy-mm-dd" id="datepicker_appointment" type="text" class="form-control" name="tanggal" placeholder="Tanggal">
    </div>

    <div class="form-group">
        <input type="text" class="form-control" name="jam" value="<?=date("H:i");?>" placeholder="Jam">
    </div>

    <div class="form-group">
                <select type="text" class="form-control" name="id_typeofneeds">
                    <option> - Pilih Keperluan - </option>
                    <?php if($keperluan != ""):?>
                        <?php foreach($keperluan as $row):?>
                          <option value="<?=$row->id_typeofneeds;?>"><?=$row->jenis_keperluan;?></option>
                        <?php endforeach;?>
                    <?php endif;?>
                </select>
            </div>

            <div class="form-group">
                <textarea class="form-control" name="keterangan" placeholder="Keterangan"></textarea>
            </div>

            </div>

            <div class="col-md">
                <div id="my_camera" style="width: 490px;height: 390px;border: solid 2px black"></div>
                <br/>
                <input type='file' onchange='onChooseFile(event, onFileLoad.bind(this, "my_camera"))' />
                <input type=button value="Ulang" onClick="reset_camera()">
                <input type="hidden" name="image" class="image-tag">
                <br/>
            </div>

        </div>

        <div class="row">
            <div class="col-md text-center">

                <br/>

                <button class="btn btn-success">Submit</button>

            </div>
        </div>

    </form>
  </section>
</div>

<script src="<?= base_url();?>/assets/vendor/webcamjs/webcam.min.js"></script>
<script language="JavaScript">

function start_camera() {
  Webcam.set({

      width: 490,

      height: 390,

      image_format: 'jpeg',

      jpeg_quality: 90

  });
  Webcam.attach( '#my_camera' );
}


function take_snapshot() {

    Webcam.snap( function(data_uri) {

        $(".image-tag").val(data_uri);

        // document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
        Webcam.reset( '#my_camera' );
        document.getElementById('my_camera').innerHTML = '<img src="'+data_uri+'"/>';

    } );

}

function reset_camera()
{
  document.getElementById('my_camera').innerHTML = '';
  //start_camera();
}

function onFileLoad(elementId, event) {
    document.getElementById(elementId).innerText = event.target.result;
}

function onChooseFile(event, onLoadFileHandler) {
    if (typeof window.FileReader !== 'function')
        throw ("The file API isn't supported on this browser.");
    let input = event.target;
    if (!input)
        throw ("The browser does not properly implement the event object");
    if (!input.files)
        throw ("This browser does not support the `files` property of the file input.");
    if (!input.files[0])
        return undefined;
    let file = input.files[0];
    var reader = new FileReader();
    reader.onload = function () {
        var dataURL = reader.result;
        var output = document.getElementById('my_camera');
        output.innerHTML = '<img src="'+dataURL+'" style="width: 490px;height: 390px;"/>';

        $(".image-tag").val(dataURL); // masukan isi gambar ke form biar bisa dikirm
    };
    reader.readAsDataURL(file);

}
</script>