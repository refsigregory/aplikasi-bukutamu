<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <section class="content-header">
    <h1 class="h3 mb-4 text-gray-800">Ubah Pengguna</h1>
  </section>
  <section>
  <p class="login-box-msg">
      <?php
          if (!empty($this->session->flashdata('msg'))):
              $msg = $this->session->flashdata('msg');
      ?>
      <?php if($msg['type'] == 'success'): ?>
          <div class="alert alert-success"><?=$msg['message'];?></div>
      <?php elseif ($msg['type'] == 'warning'): ?>
          <div class="alert alert-warning"><?=$msg['message'];?></div>
      <?php elseif ($msg['type'] == 'error'): ?>
          <div class="alert alert-danger"><?=$msg['message'];?></div>
      <?php else: ?>
          <div class="alert alert-info"><?=$msg['message'];?></div>
      <?php endif; ?>
      <?php endif; ?>
  </p>
  <form method="post" action="<?=base_url('admin/editPengguna');?>">
  <input type="hidden" name="id_user" value="<?=$data->id_user;?>" placeholder="">
  <div class="form-group">
    <label>Username</label>
    <input type="text" class="form-control" name="username" value="<?=$data->username;?>" placeholder="">
  </div>
  <div class="form-group">
    <label>Password</label>
    <input type="password" class="form-control" name="password" value="<?=$data->password;?>" placeholder="">
  </div>
  <div class="form-group">
    <label>Peran</label>
    <select class="form-control" name="id_usertype" id="tambahPeran">
      <option value="<?=$data->id_usertype;?>"> <?=$this->m_pengguna->getUsertypeByID($data->id_usertype);?> </option>
      <option value="1">Admin</option>
      <option value="4">Kepala Dinas/ Sekretaris / Kepala Bidang</option>
      <option value="3">Tamu Terdaftar</option>
      <option value="2">Tamu</option>
    </select>
  </div>

  <div id="formTambah">

    <div class="form-group"><label>Nama</label><input type="text" class="form-control" name="nama" value=<?=$data->nama;?> placeholder=""></div>

  <?php if($data->id_usertype == 3):?>
    <div class="form-group"><label>Jenis Kelamin</label><select name="jenis_kelamin" type="text" class="form-control"><option> - Jenis Kelamin - </option> <option <?=($data->jenis_kelamin=="Laki-laki") ? 'selected="selected"' : '';?>>Laki-laki</option><option <?=($data->jenis_kelamin=="Perempuan") ? 'selected="selected"' : '';?>>Perempuan</option></select></div>
    <div class="form-group"><label>Alamat</label><input type="text" name="alamat" class="form-control" value="<?=$data->alamat;?>"></div>
    <div class="form-group"><label>Asal Instansi</label><input type="text" name="asal_instansi" class="form-control" value="<?=$data->asal_instansi;?>"></div>
    <div class="form-group"><label>Email</label><input type="email" name="email" class="form-control" value="<?=$data->email;?>"></div>
    <div class="form-group"><label>Telepon</label><input type="text" name="telepon" class="form-control" value="<?=$data->telepon;?>"></div>
  <?php endif;?>

  <?php if($data->id_usertype == 4):?>
    <div class="form-group"><label>Jabatan</label><select name="id_job" class="form-control"><option value=""> - Pilih Jabatan - </option><option value="1" <?=($data->id_job==1) ? 'selected="selected' : '';?>> Kepala Dinas</option><option value="3" <?=($data->id_job==3) ? 'selected="selected' : '';?>>Sektretaris</option><option value="2" <?=($data->id_job==2) ? 'selected="selected' : '';?>>Kepala Bidang</option></select></div>
  </div>
  <?php endif;?>
  </div>

  <button type="submit" class="btn btn-primary btn-block">Simpan</button>
</form>
  </section>
</div>