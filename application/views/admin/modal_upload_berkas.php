
  <div class="modal fade" id="show-modal">
  <div class="modal-dialog">
    <div class="modal-content">
    <?=form_open_multipart(base_url('admin/uploadBerkas'));?>
        <input type="hidden" name="id_visitation" value="<?=$this->input->get('berkas');?>">
      <div class="modal-header">
        <h4 class="modal-title">Upload Berkas</h4>
        
      </div>
      <div class="modal-body">
            <label><b>Berkas</b></label>
            <input type="file" name="foto_berkas" class="form-controls">
      </div>  
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary pull-right">Simpan</button>
      </div>
    </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->