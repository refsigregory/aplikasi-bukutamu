<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <section class="content-header">
    <h1 class="h3 mb-4 text-gray-800">Daftar Appointment</h1>
  </section>
  <section>
    <!-- DataTales -->
    <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary float-left">Tamu</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                    <th>No</th>
                      <th>Tanggal</th>
                      <th>Nama Tamu</th>
                      <th>Jam Bertemu</th>
                      <th>Asal Instansi</th>
                      <th>Bertemu Dengan</th>
                      <th>Jenis Keperluan</th>
                      <th>Keperluan</th>
                      <th>Status</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; if($daftar_janji != ""): foreach($daftar_janji as $row):?>
                      <td><?=$no;?></td>
                      <td><?=$row->tanggal;?></td>
                      <td><?=$this->m_pengguna->getByID($row->id_user)[0]->nama;?></td>
                      <td><?=$row->jam;?></td>
                      <td><?=$this->m_pengguna->getByID($row->id_user)[0]->asal_instansi;?></td>
                      <td><?=$this->m_pengguna->getByID($row->id_user_pimpinan)[0]->nama;?></td>
                      <td><?=$this->m_tamu->getKeperluanByID($row->id_typeofneeds)[0]->jenis_keperluan;?></td>
                      <td><?=$row->keterangan;?></td>
                      <td><?=$row->status_permintaan;?></td>
                      <td>
                            <?php if($this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'pimpinan'):?>
                            <a href="<?=base_url('admin/terimaAppointment?id='.$row->id_appointment);?>">
                            <button class="btn btn-sm btn-success" title="Terima"><i class="fa fa-check"></i></button>
                            </a>

                            <a href="<?=base_url('admin/tolakAppointment?id='.$row->id_appointment);?>">
                            <button class="btn btn-sm btn-danger" title="Tolak"><i class="fa fa-window-close"></i></button>
                            </a>
                            <?php endif;?>
                      </td>
                    </tr>
                    <?php $no++; endforeach; endif;?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
  </section>
</div>