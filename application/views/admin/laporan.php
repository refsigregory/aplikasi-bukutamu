<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <section class="content-header">
    <h1 class="h3 mb-4 text-gray-800">Laporan</h1>
  </section>
  <section>
  <form action="<?=base_url('admin/printLaporan');?>" target="_blank">
	<div class="form-group">
		<label>Bulan</label>:
		<?php
			$bulan = [
				"Januari",
				"Februari",
				"Maret",
				"April",
				"Mei",
				"Juni",
				"Juli",
				"Agustus",
				"September",
				"Oktober",
        "November",
        "Desember"
			];
		?>
		<select name="bulan" class="">
			<?php for($i=0; $i< count($bulan); $i++):?>
				<option value="<?=$i+1;?>"><?=date("M", strtotime(date("Y") . "-" . ($i+1) . "-" . date("d")));?></option>
			<?php endfor;?>
		</select>
	</div>
	<div class="form-group">
		<label>Tahun</label>:
		<select name="tahun" class="">
			<?php for($tahun = date("Y"); $tahun>2000; $tahun--):?>
				<option><?=$tahun;?></option>
			<?php endfor;?>
		</select>
	</div>
	<div class="form-group">
		<button class="btn btn-primary">Cetak Laporan</button>
	</div>
</form>
  </section>
</div>