<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <section class="content-header">
    <h1 class="h3 mb-4 text-gray-800">Ubah Sandi</h1>
  </section>
  <section>
  <p class="login-box-msg">
      <?php
          if (!empty($this->session->flashdata('msg'))):
              $msg = $this->session->flashdata('msg');
      ?>
      <?php if($msg['type'] == 'success'): ?>
          <div class="alert alert-success"><?=$msg['message'];?></div>
      <?php elseif ($msg['type'] == 'warning'): ?>
          <div class="alert alert-warning"><?=$msg['message'];?></div>
      <?php elseif ($msg['type'] == 'error'): ?>
          <div class="alert alert-danger"><?=$msg['message'];?></div>
      <?php else: ?>
          <div class="alert alert-info"><?=$msg['message'];?></div>
      <?php endif; ?>
      <?php endif; ?>
  </p>
  
  <form method="post" action="<?=base_url('admin/ubahSandi');?>">
  <div class="form-group">
    <label>Kata Sandi Lama</label>
    <input type="password" class="form-control" name="old_password" placeholder="">
  </div>
  <div class="form-group">
    <label>Kata Sandi Baru</label>
    <input type="password" class="form-control" name="new_password" placeholder="">
  </div>
  <div class="form-group">
    <label>Konfirmasi Kata Sandi</label>
    <input type="password" class="form-control" name="konfirmasi_password" placeholder="">
  </div>
  <div id="formTambah">
  </div>
  <button type="submit" class="btn btn-primary btn-block">Ubah Sandi</button>
</form>
  </section>
</div>