<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tamu extends CI_Model
{
    
    public function getAll()
    {
        $query=$this->db->query("select * from tb_visitation");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getListByMonthAndYear($month,$year)
    {
        $query=$this->db->query("select * from tb_visitation where month(tanggal) = '$month' and year(tanggal) = '$year'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getByID($id)
    {
        $query=$this->db->query("select * from tb_visitation where id_visitation = '$id'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getRiwayatAll()
    {
        $query=$this->db->query("select * from tb_visitation where status_respon = 'finished' order by jam_masuk");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getListAll()
    {
        $query=$this->db->query("select * from tb_visitation order by jam_masuk");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getListTodayAll()
    {
        $query=$this->db->query("select * from tb_visitation where tanggal = '".date("Y-m-d")."'  order by jam_masuk");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getCountListAll()
    {
        $query=$this->db->query("select * from tb_visitation where status_respon = 'pending' and tanggal = '".date("Y-m-d")."'  order by jam_masuk");
        return $query->num_rows();
    }

    public function getCountListTodayAll()
    {
        $query=$this->db->query("select * from tb_visitation where status_respon = 'pending' and tanggal = '".date("Y-m-d")."' order by jam_masuk");
        return $query->num_rows();
    }

    public function getCountListTamuToday()
    {
        $query=$this->db->query("select * from tb_visitation where status_respon = 'finished' and tanggal = '".date("Y-m-d")."' order by jam_masuk");
        return $query->num_rows();
    }

    public function getCountListAntrianToday()
    {
        $query=$this->db->query("select * from tb_visitation where status_respon = 'pending' and tanggal = '".date("Y-m-d")."' order by jam_masuk");
        return $query->num_rows();
    }

    public function getLast()
    {
        $this->db->query("UPDATE tb_visitation SET status_respon = 'canceled' WHERE status_respon = 'pending' AND id_appointment != '' AND CONCAT(tanggal, ' ', jam_masuk) < NOW()"); // reset antrian yang sudah lewat hari
        $query=$this->db->query("select * from tb_visitation where status_respon = 'pending' order by jam_masuk limit 1"); // ambil antrian tamu
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getLastToday()
    {
        $this->db->query("UPDATE tb_visitation SET status_respon = 'canceled' WHERE status_respon = 'pending' AND id_appointment != '' AND CONCAT(tanggal, ' ', jam_masuk) < DATE_ADD(NOW(), INTERVAL 1 HOUR)"); // reset antrian yang sudah lewat hari dengan toleransi 1 jam
        $query=$this->db->query("select * from tb_visitation where status_respon = 'pending' and tanggal = '".date("Y-m-d")."' order by jam_masuk limit 1"); // ambil antrian tamu
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getRiwayatByKepala($id)
    {
        $query=$this->db->query("select * from tb_visitation where id_user_pimpinan = '$id' and status_respon = 'finished' order by jam_masuk");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getListByKepala($id)
    {
        $query=$this->db->query("select * from tb_visitation where id_user_pimpinan = '$id' order by jam_masuk");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getListByKepalaToday($id)
    {
        $query=$this->db->query("select * from tb_visitation where id_user_pimpinan = '$id' AND tanggal = '".date("Y-m-d")."' and status_respon = 'pending' order by jam_masuk");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getCountListByKepala($id)
    {
        $query=$this->db->query("select * from tb_visitation where id_user_pimpinan = '$id' order by jam_masuk");
        return $query->num_rows();
    }

    public function getCountListTodayByKepala($id)
    {
        $query=$this->db->query("select * from tb_visitation where id_user_pimpinan = '$id' AND tanggal = '".date("Y-m-d")."' and status_respon = 'pending'  order by jam_masuk");
        return $query->num_rows();
    }

    public function getLastByKepala($id)
    {
        $query=$this->db->query("select * from tb_visitation where id_user_pimpinan = '$id' and status_respon = 'pending' order by jam_masuk limit 1");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getLastTodayByKepala($id)
    {
        $query=$this->db->query("select * from tb_visitation where id_user_pimpinan = '$id' and status_respon = 'pending'  AND tanggal = '".date("Y-m-d")."' and status_respon = 'pending'  order by jam_masuk limit 1");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function setStatus($status, $id_tamu)
    {
        $this->db->where("id_visitation", $id_tamu);
        $query = $this->db->update("tb_visitation", ["status_respon"=>$status]);
        return $query;
    }

    public function setStatusKeberadaan($status, $id)
    {
        $this->db->where("id_user", $id);
        $query = $this->db->update("tb_user", ["status_keberadaan"=>$status]);
        return $query;
    }

    public function getAppointmentByID($id)
    {
        $query=$this->db->query("select * from tb_appointment where id_appointment = '$id' order by id_appointment desc");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getAppointment()
    {
        $query=$this->db->query("select * from tb_appointment where id_appointment order by id_appointment desc");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getListAppointment()
    {
        $query=$this->db->query("select * from tb_appointment where id_appointment order by id_appointment desc");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getListAppointmentByKepala($id)
    {
        $query=$this->db->query("select * from tb_appointment where id_user_pimpinan = '$id' AND status_permintaan = 'pending' order by id_appointment desc");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getListAppointmentByPengguna($id)
    {
        $query=$this->db->query("select * from tb_appointment where id_user = '$id' AND status_permintaan = 'pending' OR status_permintaan = 'accepted' order by id_appointment desc");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function setAppointment($status, $id_appointment)
    {
        $this->db->where("id_appointment", $id_appointment);
        $query = $this->db->update("tb_appointment", ["status_permintaan"=>$status]);
        return $query;
    }

    public function getPimpinan()
    {
        $query = $this->db->query("select * from tb_user where id_usertype = 4");
        if($query->num_rows() > 0)
        {
            return $query->result();
        }else {
            return "";
        }
    }

    public function getKeperluan()
    {
        $query = $this->db->query("select * from tb_typeofneeds");
        if($query->num_rows() > 0)
        {
            return $query->result();
        }else {
            return "";
        }
    }

    public function getKeperluanByID($id)
    {
        $query = $this->db->query("select * from tb_typeofneeds where id_typeofneeds = '$id'");
        if($query->num_rows() > 0)
        {
            return $query->result();
        }else {
            return "";
        }
    }

    public function update($id,$data)
    {
        $this->db->where("id_visitation", $id);
        $query = $this->db->update("tb_visitation", $data);
        return $query;
    }
        
}