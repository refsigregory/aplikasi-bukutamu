<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_auth extends CI_Model {
    public function __construct()
    {
        parent::__construct();
    }

    public function check()
    {
        // cek jika sudah login
        if(!$this->session->userdata('logged_in')){
            $msg = "Anda harus login dahulu!";
            $this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
            redirect('auth/login', 'refresh');
        }
    }

    public function blockRole($role)
    {
        // cek jika sudah login
        if($this->session->userdata('role') == $role){
            $msg = "Anda tidak memiliki akses";
            $this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
            if($role == 'tamu'):
                redirect('auth/tamu', 'refresh');
            else:
                redirect('admin', 'refresh');
            endif;
        }
    }

    public function logged_in()
    {
        // cek jika sudah login
        if($this->session->userdata('logged_in')){
            if($this->session->userdata('role') != 'tamu')
                {
                    redirect('admin', 'refresh');
                } else {
                    redirect('auth/tamu', 'refresh');
                }
        }
    }

    public function login($username = null, $password = null)
    {
        // validasi login
        if($username != null && $password != null)
        {
            // jika username dan password tidak kosong
            $query = $this->db->query("select * from tb_user where username='$username' AND password='$password'");
            if($query->num_rows()>0){
                // jika login sukses
            if($query->row()->status_pendaftaran == 'terverifikasi'):
                switch($query->row()->id_usertype):
                    case 1:
                        $role = "admin";
                    break;
                    case 3:
                        $role = "pengguna";
                    break;
                    case 4:
                        $role = "pimpinan";
                    break;
                    case 2:
                        $role = "tamu";
                    break;
                    default;
                    $role = "";
                endswitch;
                $data = ['id' => $query->row()->id_user,'id_user' => $query->row()->id_user,'username' => $query->row()->username,'name' => $query->row()->username, 'role' => $role, 'logged_in'=>true];
                $this->session->set_userdata($data);
                
                if($role != 'tamu')
                {
                    redirect('admin', 'refresh');
                } else {
                    redirect('auth/tamu', 'refresh');
                }
            else:
                $msg = "Akun anda belum aktif, silahkan klil link aktivasi yang dikirim ke email anda!";
                $this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
                redirect('auth/login', 'refresh');
            endif;

            }else {
                // login gagal
                $msg = "Username atau Password salah!";
                $this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
                redirect('auth/login', 'refresh');
            }
        }else {
            $msg = "Username dan Password tidak boleh kosong!";
            $this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
            redirect('auth/login', 'refresh');
        }
    }

    public function getPimpinan()
    {
        $query = $this->db->query("select * from tb_user where id_usertype = 3");
        if($query->num_rows() > 0)
        {
            return $query->result();
        }else {
            return "";
        }
    }

    private function clean($str)
    {
        // fungsi escape string
        return $str;
    }
}