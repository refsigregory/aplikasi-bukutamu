<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pengguna extends CI_Model
{
    public function getAll()
    {
        $this->db->select('*');
        $this->db->from('tb_user');
        $query=$this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getByID($id)
    {
        $this->db->select('*');
        $this->db->from('tb_user');
        $this->db->where('id_user', $id);
        $query=$this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getPengguna()
    {
        $this->db->select('*');
        $this->db->from('tb_user');
        $query=$this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getPenggunaByID($id)
    {
        $this->db->select('*');
        $this->db->from('tb_user');
        $this->db->where('id_user', $id);
        $query=$this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getPenggunaByIDUser($id)
    {
        $this->db->select('*');
        $this->db->from('tb_user');
        $this->db->where('id_user', $id);
        $query=$this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }
    public function getAdminByIDUser($id)
    {
        $this->db->select('*');
        $this->db->from('tb_user');
        $this->db->where('id_user', $id);
        $query=$this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }
    public function getKepalaByIDUser($id)
    {
        $this->db->select('*');
        $this->db->from('tb_user');
        $this->db->where('id_user', $id);
        $query=$this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }
    
    public function getUsertypeByID($id)
    {
        $this->db->select('*');
        $this->db->from('tb_usertype');
        $this->db->where('id_usertype', $id);
        $query=$this->db->get();
        if($query->num_rows()>0)
        {
            return $query->row()->tipe_user;
        } else {
            return "";
        }
    }

    public function getJobByID($id)
    {
        $this->db->select('*');
        $this->db->from('tb_job');
        $this->db->where('id_job', $id);
        $query=$this->db->get();
        if($query->num_rows()>0)
        {
            return $query->row()->jabatan;
        } else {
            return "";
        }
    }

    public function getNotifikasiByIDUser($id)
    {
        $query=$this->db->query("select * from tb_notification where id_user = '$id' and status_dibaca = 0");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getCountNotifikasiByIDUser($id)
    {
        $query=$this->db->query("select count(*) as jumlah from tb_notification where id_user = '$id' and status_dibaca = 0");
        if($query->num_rows()>0)
        {
            return $query->row()->jumlah;
        } else {
            return 0;
        }
    }

    public function tambah($data)
    {
        if($data['id_usertype'] != ""){
            $query = $this->db->insert("tb_user", $data);

            if($query) {
                $msg = "Pengguna berhasil ditambah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
                redirect('admin/pengguna');
            } else {
                $msg = "Kesalahan saat menambah user";
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('admin/tambah_pengguna?error='.$msg, 'refresh');
                redirect('admin/tambah_pengguna?error=Kesalahan saat menambah user');
            }
        } else {
            $msg = "Mohon memilih Peran";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
            redirect('admin/tambah_pengguna?error=Mohon memilih Peran');
        }
    }

    public function edit($id,$data)
    {
        if($data['id_usertype'] != ""){
            $this->db->where("id_user", $id);
            $query = $this->db->update("tb_user", $data);

            if($query) {
                $msg = "Pengguna berhasil diubah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
                redirect('admin/pengguna');
            } else {
                $msg = "Kesalahan saat memproses user";
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('admin/edit_pengguna?id='.$data['id_user'].'&error='.$msg, 'refresh');
                redirect('admin/edit_pengguna?id='.$data['id_user'].'&error=Kesalahan saat menambah user');
            }
        } else {
            $msg = "Mohon memilih Peran";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
            redirect('admin/editeditpengguna?id='.$data['id_user'].'&error=Mohon memilih Peran');
        }
    }

    public function delete($id)
    {
        $data = $this->getByID($id);
        if($data !="")
        {
            $this->db->where('id', $id);
            $query = $this->db->delete("tb_user");
            if($query){
                    $this->db->where('id_user', $id);
                    $query = $this->db->delete("tb_user");
                return $query;
            }
        }else {
            $query = false;
        }
    }

    public function insertNotification($id_user, $notif)
    {
        $data = ["id_user"=>$id_user, "pesan"=>$notif];
        $query = $this->db->insert("tb_notification", $data);
        return $query;
    }
}